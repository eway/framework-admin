import querystring from 'querystring'
import storage from 'localStorage'
import toastr from 'toastr'
import axios from 'axios'
import Vue from 'vue'

const baseURL = 'http://localhost:8080'  // 所有请求的根路径

export const http = axios.create({
  baseURL: baseURL,
  timeout: 10000,
  withCredentials: true,
  headers: {'X-Requested-With': 'XMLHttpRequest'},
  transformRequest: [data => {
    return querystring.stringify(data)
  }]
})

// Add a response interceptor handing global errors
http.interceptors.response.use(response => {
  return response
}, error => {
  var response = error.response
  if (!response) {
    return toastr.info('服务器太久没有响应, 请重试!')
  }
  console.log(response)
  toastr.error(response.data.msg || '请求发生未知错误, 请联系系统管理员 *_*!!')
  return Promise.reject(error)
})

Vue.prototype.$http = http  // 为vue实例添加`$http`属性, 方便组件内随时调用

export const Auth = {
  authorizationCheck() { // 登录验证
    return new Promise((resolve, reject) => {
      try {
        http.get('/authorization-check').then(resolve, reject)
      } catch (e) { // 验证失败后台会抛出401异常, 注销当前用户
        console.error('authorizationCheck', e)
        toastr.error('会话已过期, 请重新登录.')
        storage.removeItem('user')
        reject(response)
      }
    })
  },
  login: (data) => {
    return new Promise((resolve, reject) => {
      http.post('/login', data).then(response => {
        if (response.data.success) {
          storage.setItem('user', JSON.stringify(response.data.user))
        }
        resolve(response)
      }, reject).catch(reject)
    })
  },
  logout: (to, from, next) => {
    storage.removeItem('user')
    http.get('/logout')
    next('/login')
  },
  isLogin() {
    try {
      return User.current()
    } catch (e) {
      toastr.error('用户信息出错，请重新登录!')
    }
  }
}

export const Department = resource('department', http, {
  treetable: () => http.get('department/treetable'),
  jstree: (selected) => http.get('department/jstree', {params: {selected: [].concat(selected || '')}}),  // 获取所有部门
  tree: (map) => http.get('department/tree?map=' + !!map),  // map为true时额外返回一个{id -> 部门}的映射对象
  all: () => http.get('department/all')  // 获取所有部门
})

export const Permission = resource('permission', http, {
  treetable: () => http.get('permission/treetable'),
  perms: () => http.get('permission/perms'),
  jstree: () => http.get('permission/jstree'),
  tree: () => http.get('permission/tree')
})

export const User = resource('user', http, {
  current: request => {
    if (request) {
      return http.get('user/current')
    }
    var value = storage.getItem('user')
    if (value) {
      return JSON.parse(value)
    }
  },
  paging: params => http.get('user', {params}),
  query: params => http.get('user/query', {params}),
  /*
   * 搜索用户
   * keyword        搜索关键字
   * type           过滤用户类型
   * dept           过滤部门ID
   * include        需包含的用户
   * excludeCurrent 是否排除当前用户
   */
  search: params => http.get('user/search', {params}),
  roles: (id = '') => http.get('user/roles?id=' + id),  // 获取所有用户
  profile (user) {
    if (user) {
      return http.put('user/profile', user)
    }
    return http.get('user/profile')
  },
  status: (id, status) => http.put('user/status', {id, status}),  // 更新状态
  role: (id, roleId) => http.put('user/role', {id, roleId}),      // 更新角色
  partial: (params) => http.put('user/partial', params)           // 局部更新用户信息
})

export const Role = resource('role', http, {
  owns: (id) => http.get('role/owns?id=' + id || ''),     // 获取角色所有权限ID
  perms: (id) => http.get('role/perms?id=' + id),         // 获取角色所有权限字符串
  put_perms: (params) => http.put('role/perms', params),  // 保存角色权限更改
  all: () => http.get('role/all'),                        // 获取所有角色
  map: () => http.get('role/map')                         // 获取 角色ID=>角色 的映射对象
})

export const Dict = resource('dict', http, {
  // save 与 update 方法需要传递复杂的JSON对象
  // 因此不能以默认的`application/x-www-form-urlencoded`方式传递参与
  // 要修改为`application/json`并以JSON字符串传递, 后端才可以接收整个JSON对象
  save: (data) => jsonRequest('/dict', 'post', data),
  update: (data) => jsonRequest('/dict', 'put', data),

  bycode: (code) => http.get('dict/bycode?code=' + code)  // 按字典编码查询单个字典对象
})

/**
 * send reqeust with Content-Type: `application/json`
 * @param path request path
 * @param method  request method
 * @param data JSON data
 * @returns {AxiosPromise}
 */
export const jsonRequest = function (path, method, data) {
  return http[method](path, data, {
    headers: {'Content-Type': 'application/json'},
    transformRequest: [data => {
      return JSON.stringify(data)
    }]
  })
}

/**
 * create vue-resource's resource like object
 *
 * Default Actions
 *   get: {method: 'GET'}
 *   save: {method: 'POST'}
 *   query: {method: 'GET'}
 *   update: {method: 'PUT'}
 *   delete: {method: 'DELETE'}
 *
 * @param path the resource path
 * @param http axios instance
 * @param actions custom actions
 * @returns the resource object
 */
function resource (path, http, actions) {
  var obj = {
    get: id => http.get(path + '/' + id),
    save: obj => http.post(path, obj),
    query: params => http.get(path, {params}),
    update: obj => http.put(path, obj),
    delete: id => http.delete(path + '/' + id)
  }
  return Object.assign(obj, actions)
}